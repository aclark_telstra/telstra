angular.module('myapp.deviceDescription', [])
	.config(['c8yViewsProvider', function (c8yViewsProvider) {
		c8yViewsProvider.when('/device/:deviceId', {
			name: 'Description',
			icon: 'book',
			priority: 10,
			templateUrl: ':::PLUGIN_PATH:::/views/deviceDescription.html',
			controller: 'deviceDescriptionCtrl'
		});
	}]);